export function fight(firstFighter, secondFighter) {
  let firstFighterHealth = firstFighter.health;
  let secondFighterHealth = secondFighter.health;

  while (true) {
    secondFighterHealth -= getDamage(firstFighter, secondFighter);
    
    if (secondFighterHealth <= 0) {
      return firstFighter;
    }
    
    firstFighterHealth -= getDamage(secondFighter, firstFighter);
    
    if (firstFighterHealth <= 0) {
      return secondFighter;
    }
  }
}

export function getDamage(attacker, enemy) {
  return getHitPower(attacker) - getBlockPower(enemy);
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}