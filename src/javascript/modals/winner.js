import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showWinnerModal(fighter) {
  const nameElement = createElement({ tagName: 'div', className: 'winner-name' });
  nameElement.innerText = fighter.name;

  const imageElement = createElement({ tagName: 'img', className: 'modal-field' });
  imageElement.src = fighter.source;

  const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
  bodyElement.insertAdjacentElement('beforeend', nameElement);
  bodyElement.insertAdjacentElement('beforeend', imageElement);

  showModal({ title: 'Winner', bodyElement: bodyElement });
}