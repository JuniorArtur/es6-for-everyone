import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'modal-field' });
  const attackElement = createElement({ tagName: 'span', className: 'modal-field' });
  const defenseElement = createElement({ tagName: 'span', className: 'modal-field' });
  const healthElement = createElement({ tagName: 'span', className: 'modal-field' });
  const imageElement = createElement({ tagName: 'img', className: 'modal-field' });

  if (name) {
    nameElement.innerText = 'Name: ' + name;
    fighterDetails.append(nameElement);
  }
  if (attack) {
    attackElement.innerText = 'Attack: ' + attack;
    fighterDetails.append(attackElement);
  }
  if (defense) {
    defenseElement.innerText = 'Defense: ' + defense;
    fighterDetails.append(defenseElement);
  }
  if (health) {
    healthElement.innerText = 'Health: ' + health;
    fighterDetails.append(healthElement);
  }
  if (source) {
    imageElement.src = source;
    fighterDetails.append(imageElement);
  }
  return fighterDetails;
}
